import 'babel-polyfill';

import './scss/main.scss';
import './scss/print.scss';
import './scss/ckeditor.scss';

// init controller
// const controller = new ScrollMagic.Controller();

// build scene
//var scene = new ScrollMagic.Scene({
//  triggerElement: "#block-prius-worldmap"
//})
//.setTween("#block-prius-worldmap *[cc='kh']", 0.5, {backgroundColor: "green"}) // trigger a TweenMax.to tween
//.addIndicators({name: "1 (duration: 0)"}) // add indicators (requires plugin)
//.addTo(controller);

(function ($) {
  const countriesMapEn = {
    cn: { url: '/asia/china' },
    in: { url: '/asia/india' },
    th: { url: '/asia/thailand' },
    mm: { url: '/asia/myanmar' },
    kh: { url: '/asia/cambodia' },
    vn: { url: '/asia/vietnam' },
    id: { url: '/asia/indonesia' },
    au: { url: '/australia/australia' },
    nz: { url: '/australia/new-zealand' },
    mx: { url: '/north-america/mexico' },
    cr: { url: '/south-america/costa-rica' },
    ar: { url: '/south-america/argentina' },
    cl: { url: '/south-america/chile' },
    br: { url: '/south-america/brazil' },
    pe: { url: '/south-america/peru' },
    za: { url: '/africa/south-africa' },
  };

  const countriesMapFr = {
    mx: { url: '/fr/amerique-du-nord/mexique' },
    cr: { url: '/fr/amerique-du-sud/costa-rica' },
    cn: { url: '/fr/asie/chine' },
    in: { url: '/fr/asie/inde' },
    au: { url: '/fr/australie/australie' },
    nz: { url: '/fr/australie/nouvelle-zelande' },
    za: { url: '/fr/afrique/afrique-du-sud' },
    kh: { url: '/fr/asie/cambodge' },
    mm: { url: '/fr/asie/myanmar' },
    th: { url: '/fr/asie/thailande' },
    vn: { url: '/fr/asie/vietnam' },
    id: { url: '/fr/asie/indonesie' },
    ar: { url: '/fr/amerique-du-sud/argentine' },
    br: { url: '/fr/amerique-du-sud/bresil' },
    pe: { url: '/fr/amerique-du-sud/perou' },
  };

  // /fr/amerique-du-sud/bolivie
  // /fr/amerique-du-sud/colombie
  // /fr/afrique/madagascar

  Drupal.behaviors.worldmap = {
    attach: () => {
      // English.
      Object.keys(countriesMapEn).forEach((key) => {
        $(`.worldmap-en [cc='${key}']`).hover(function () {
          console.log('worldmapen');
          $(this).css('fill', '#B8A242');
        }, function () {
          $(this).css('fill', '#006064');
        }).click(() => {
          window.location = countriesMapEn[key].url;
        });
      });
      // French.
      Object.keys(countriesMapFr).forEach((key) => {
        $(`.worldmap-fr [cc='${key}']`).hover(function () {
          console.log('worldmapfr');
          $(this).css('fill', '#B8A242');
        }, function () {
          $(this).css('fill', '#006064');
        }).click(() => {
          window.location = countriesMapFr[key].url;
        });
      });
    },
  };

  Drupal.behaviors.article = {
    attach: () => {
      $('#article__tldr button').click(() => {
        $('#article__tldr').addClass('open');
      });
    },
  };

}(jQuery));

(function ($) {

  'use strict';

  var docElem = window.document.documentElement;

  window.requestAnimFrame = function () {
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function (/* function */ callback) {
        window.setTimeout(callback, 1000 / 60);
      }
    );
  }();

  window.cancelAnimFrame = function () {
    return (
      window.cancelAnimationFrame ||
      window.webkitCancelAnimationFrame ||
      window.mozCancelAnimationFrame ||
      window.oCancelAnimationFrame ||
      window.msCancelAnimationFrame ||
      function (id) {
        window.clearTimeout(id);
      }
    );
  }();

  function SVGEl(el) {
    this.el = el;
    this.image = this.el.previousElementSibling;
    this.current_frame = 0;
    this.total_frames = 180;
    this.path = new Array();
    this.length = new Array();
    this.handle = 0;
    this.init();
  }

  SVGEl.prototype.init = function () {
    var self = this;
    [].slice.call(this.el.querySelectorAll('path')).forEach(function (path, i) {
      self.path[i] = path;
      var l = self.path[i].getTotalLength();
      self.length[i] = l;
      self.path[i].style.strokeDasharray = l + ' ' + l;
      self.path[i].style.strokeDashoffset = l;
    });
  };

  SVGEl.prototype.render = function () {
    if (this.rendered) return;
    this.rendered = true;
    this.draw();
  };

  SVGEl.prototype.draw = function () {
    var self = this,
      progress = this.current_frame / this.total_frames;
    if (progress > 1) {
      window.cancelAnimFrame(this.handle);
      this.showImage();
    } else {
      this.current_frame++;
      for (var j = 0, len = this.path.length; j < len; j++) {
        this.path[j].style.strokeDashoffset = Math.floor(this.length[j] * (1 - progress));
      }
      this.handle = window.requestAnimFrame(function () { self.draw(); });
    }
  };

  SVGEl.prototype.showImage = function () {
    $(this.image).attr("class", "drawing-illustration show");
    $(this.el).attr("class", "drawing-line hide");
  };

  function getViewportH() {
    var client = docElem['clientHeight'],
      inner = window['innerHeight'];

    if (client < inner)
      return inner;
    else
      return client;
  }

  function scrollY() {
    return window.pageYOffset || docElem.scrollTop;
  }

  // http://stackoverflow.com/a/5598797/989439
  function getOffset(el) {
    var offsetTop = 0, offsetLeft = 0;
    do {
      if (!isNaN(el.offsetTop)) {
        offsetTop += el.offsetTop;
      }
      if (!isNaN(el.offsetLeft)) {
        offsetLeft += el.offsetLeft;
      }
    } while (el = el.offsetParent)

    return {
      top: offsetTop,
      left: offsetLeft
    };
  }

  function inViewport(el, h) {
    var elH = el.offsetHeight,
      scrolled = scrollY(),
      viewed = scrolled + getViewportH(),
      elTop = getOffset(el).top,
      elBottom = elTop + elH,
      // if 0, the element is considered in the viewport as soon as it enters.
      // if 1, the element is considered in the viewport only when it's fully inside
      // value in percentage (1 >= h >= 0)
      h = h || 0;
    return (elTop + elH * h) <= viewed && (elBottom) >= scrolled;
  }

  function init() {

    var svgs = Array.prototype.slice.call(document.querySelectorAll('.drawing svg')),
      svgArr = new Array(),
      didScroll = false,
      resizeTimeout;

    // the svgs already shown...
    svgs.forEach(function (el, i) {
      var svg = new SVGEl(el);
      svgArr[i] = svg;
      setTimeout(function (el) {
        return function () {
          if (inViewport(el.parentNode)) {
            svg.render();
          }
        };
      }(el), 500);
    });

    var scrollHandler = function () {
      if (!didScroll) {
        didScroll = true;
        setTimeout(function () { scrollPage(); }, 60);
      }
    },
      scrollPage = function () {
        svgs.forEach(function (el, i) {
          if (inViewport(el.parentNode, 0.5)) {
            svgArr[i].render();
          }
        });
        didScroll = false;
      },
      resizeHandler = function () {
        function delayed() {
          scrollPage();
          resizeTimeout = null;
        }
        if (resizeTimeout) {
          clearTimeout(resizeTimeout);
        }
        resizeTimeout = setTimeout(delayed, 200);
      };

    window.addEventListener('scroll', scrollHandler, false);
    window.addEventListener('resize', resizeHandler, false);
  }

  $(document).ready(function ($) {
    init();
  });

})(jQuery);
